import { IContext } from "../utils";

export const Post = {
  author: ({ id }, args, ctx: IContext) => {
    return ctx.prisma.post({ id }).author();
  },
};
